======================================
Single Class Classification
======================================

The single class classification uses the IsolationForest_ algorithm from sk-learn. It is essentially an outlier detection, based on the RandomForest models.

After training the model, the model determines for each pixel whether or not it belongs to the training class. An output tif with values -1 (not the
class of interest) and 1 (class of interest) is the output.

A single-class classification can be chosen in the config file with ``app_algorithm': 'singleclass'``

|

A single class classification only requires the tif to be classified and a polygon file containing 1 single class.
**NOTE** Make sure there is only 1 class in the training data, because the algorithm assumes that it is the case!

A probability map, with values ranging from -infinity to infinity can also be produced by setting
``su_probablity: true`` in the configuration file.

The Classifier will output a probability tif, where each pixel is given a certain value, depending on how probable it is to
belong to the given class.

Using the data provided, run a singleclass classification using the following command::

  docker run -v <WORKSPACE>:/workspace classifier classification --name test_singleclass --rois /workspace/rois/bra_rois_singleclass.gpkg /workspace/rasters/brazil_2018-09-24.tif


Below you can see the input image and the output image for the class 'water'

|pic1|  |pic2|

.. |pic1| image:: ../../images/brazil_input.png
   :width: 45%

.. |pic2| image:: ../../images/singleclass.png
   :width: 45%


.. _IsolationForest: https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.IsolationForest.html


