======================================
Supervised Classification
======================================
The supervised classification in Classifier can be done using either the
RandomForest_ Classfier algorithm from sk-learn, or the GradientBoosting_ algorithm from xgboost.

**Random Forest**

The multi-class classification with Random Forest can be chosen in the config file
``'app_algorithm': 'randomforest'``

In order to perform the classification, the rasters to classify as well as a file containing
the polygons that serve as ground truth must be in the folder mounted to /workspace

The file containing the polygons can either be any OGR supported format_ and must contain a column named
'id' which contains the category of each given polygon. These should be integers. The location of the file
can be specified in the command using the ``--rois`` argument when running classifier (see below).

Using the data provided, run a random forest classification using the following command::

    docker run -v <WORKSPACE>:/workspace classifier classification --name test_randomforest --rois /workspace/rois/bra_rois.gpkg /workspace/rasters/brazil_2018-09-24.tif

**Gradient Boosting**

The multi-class classification with Gradient Boosting can be chosen in the config file
``'app_algorithm': 'xgboost'``

**NOTE** This algorithm does not scale well due to a bug in the xgboost library.
We, therefore, recommend to use this on small datasets only. Make sure the ``app_threads`` parameter is set to 1 when
using this algorithm

Below you can see the input image and the output image for the three classes using the randomforest algorithm

|pic1|  |pic2| |pic3|

.. |pic1| image:: ../../images/brazil_input.png
   :width: 43%

.. |pic2| image:: ../../images/randomforest.png
   :width: 43%

.. |pic3| image:: ../../images/rflegend.png
   :width: 12%

**Dynamic Time Warping (DTW)**
``'app_algorithm': 'knn_dtw'``

`Dynamic Time Warping (DTW) <https://dtaidistance.readthedocs.io/en/latest/>`_ is another supervised classification method. 
It creates **characteristic time series for each of your classes** and uses these for a 
`K-Nearest-Neighbor (KNN) <https://scikit-learn.org/stable/modules/generated/sklearn.neighbors.KNeighborsClassifier.html>`_ classification. 
As a distance metric, dtw is used instead of euclidean distance, which also allows to consider temporal shifts. 
So if a time series is just shifted in time to the same or similiar one, dtw will still return a low distance/high similarity 
while euclidean distance would give a high value/low similarity.

Note: When you choose to use more than 1 characteristic time series per class, classifier will use a random subset of your regions for each of these time series 
to create a bit adifferent time series per class.

Below you can see the differenc between using euclidean and dtw distances

|pic4|

.. |pic4| image:: ../../images/Euclideandistance_and_Dtw.png
   :width: 20%

Below you can see the train polygons and the classification result for the four classes *Soy, Corn, Forest and Bare* using the **Knn_Dtw** algorithm.

|pic6| |pic7| |pic8|

.. |pic6| image:: ../../images/dtw_classification_input.png
   :width: 40%

.. |pic7| image:: ../../images/dtw_classification_result.png
   :width: 40%

.. |pic8| image:: ../../images/dtw_legend.png
   :width: 10%

.. _RandomForest: https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html
.. _GradientBoosting: https://xgboost.readthedocs.io/en/latest/python/python_intro.html
.. _DynamicTimeWarping: https://dtaidistance.readthedocs.io/en/latest/
.. _format: https://gdal.org/drivers/vector/index.html