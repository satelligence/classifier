======================================
Tutorials
======================================
All tutorials below require data. Example data can be downloaded for a small area in Brazil.

raster1_

raster2_

raster3_

raster4_

rois_

The imagery is preprocessed Sentinel-2 imagery from the Copernicus programme. The ground truth data
is semi-fictive data, containing 3 classes: agricultural vegetation(id=1), natural vegetation(id=2), water(id=3).


Multiple options are available to run a classification in Classifier. The choices are:

.. toctree::
    UnsupervisedClassification
    SupervisedClassification
    SingleClassSupervisedClassification

.. _raster1: https://storage.googleapis.com/s11-public/classifier_data/brazil/rasters/BRA_MT_sub_S2All_2018-09-09_mean_subset.tif
.. _raster2: https://storage.googleapis.com/s11-public/classifier_data/brazil/rasters/BRA_MT_sub_S2All_2018-09-14_mean_subset.tif
.. _raster3: https://storage.googleapis.com/s11-public/classifier_data/brazil/rasters/BRA_MT_sub_S2All_2018-09-24_mean_subset.tif
.. _raster4: https://storage.googleapis.com/s11-public/classifier_data/brazil/rasters/BRA_MT_sub_S2All_2018-09-29_mean_subset.tif
.. _rois: https://storage.googleapis.com/s11-public/classifier_data/brazil/rois/bra_rois.gpkg