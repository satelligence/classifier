======================================
Unsupervised Classification
======================================

The unsupervised classification in Classifier is done using the
Kmeans_ Classfier algorithm from sk-learn

The unsupervised classification can be chosen in the config file
``'app_algorithm': 'us_kmeans'``
or
``'app_algorithm': 'us_kmeans_minibatch'``

The minibatch version of k_means is much faster, but might be less accurate. Do some test on smaller areas if you are unsure

You can set the desired number of classes with the ``us_nclasses`` parameter in the config file

Using the data provided, run an unsupervised classification using the following command::

    docker run -v <WORKSPACE>:/workspace classifier classification --name test_unsupervised /workspace/rasters/brazil_2018-09-24.tif

Below you can see the input image and the output image for 5 classes.

|pic1|  |pic2|

.. |pic1| image:: ../../images/brazil_input.png
   :width: 43%

.. |pic2| image:: ../../images/unsupervised.png
   :width: 43%

.. _Kmeans: https://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html