======================================
Config File
======================================

There are many parameters that can (or need!) to be changed in order to get
good classification results. All these parameter have a default value, but
you will find that the defaults are not always good enough for your purpose.

To change the default parameters you need to create a config file and place
that file into your *workspace* directory. This file needs to be a nested file,
so use the header abbreviation as a section and then put the parameters in there.


You can make the file yourself and put it in the workspace directory, but you
can also let Classifier make it for you!::

    docker run -v YOUR_WORKSPACE_DIRECTORY:/workspace classifier make_config

Below you see all avalaible parameters, subdivided into different subsections.

At the end of the page, there is an example config file.


+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
| **Parameter**              | **Default**             | **Description**                                                                                                                                 |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
| **Application Parameters (json: app)**                                                                                                                                                                 |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|     algorithm              | randomforest            | The algorithm to use. Can be: randomforest, xgboost, singleclass,us_kmeans                                                                      |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|     window                 | 1024                    | Size of the windows that are processed individually by each process. Smaller windows mean less memory consumption, but slower processing times. |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|     model                  | None                    | Model file to use for prediction                                                                                                                |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
| model_save                 | False                   | Save a model file which can be re-used&lt;/pre&gt;                                                                                              |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|     samples                | None                    | Samples file to use for model training                                                                                                          |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|     log_level              | INFO                    | Logging level of the application. Set to "DEBUG" for more information during classification                                                     |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|     threads                | -1                      | Number of threads to start. When set to -1 it will automatically take the nunmber of cpus present on your platform                              |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|     imputation             | false                   | Whether to impute missing values in the data. Choose a method for imputation using the     imputation_strategy parameter.                       |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|     imputation_strategy    | mean                    | Which method to use for imputation. Choose from: mean, median, most_frequent, constant, interpolate.                                            |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|     imputation_constant    | -9999                   | The constant value to use when doing imputation of missing values with the constant value method                                                |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|     rasters_are_timeseries | false                   | Whether the input rasters are timeseries or not                                                                                                 |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
| **Supervised Algorithms (json: supervised)**                                                                                                                                                           |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|    probability             | False                   | Output Probability Map                                                                                                                          |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|    all_probabilities       | False                   | Output Probability Map for every class                                                                                                          |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|    remove_outliers         | True                    | Remove outliers from the training data                                                                                                          |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
| **Subsection of supervised: Random Forest Hyperparameter search (json: optimization)**                                                                                                                 |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|    optimize                | False                   | Optimize the model parameters                                                                                                                   |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|    optimize_number         | 10                      | Number of iterations for optimization                                                                                                           |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
| **Unsupervised Algorithms (json: unsupervised)**                                                                                                                                                       |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|    nclasses                | 2                       | Number of classes for unsupervised                                                                                                              |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|    trainfraction           | 1.0                     | Fraction of raster used for training                                                                                                            |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
| **Accuracy Assessment (json: accuracy)**                                                                                                                                                               |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|     perform_assesment      | True                    | Perform accuracy assessment                                                                                                                     |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|     testfraction           | 0.25                    | Fraction of data to use for testing, and is omitted from training                                                                               |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
| **Dynamic time warping (json: dtw)**                                                                                                                                                                   |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|     patterns               | None                    | Path if you want to provide samples files: "/workspace/..."                                                                                     |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|     patterns_save          | False                   | True if you want to save the patterns as pickel file                                                                                            |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|number_of_patterns_per_class|     1                   | How many patterns per class are created. Uses different subsets of regions for different patterns                                               |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|    n_neighbors             |    1                    | Number of Neighbors for K-Nearest Neighbors. >1 only if you have more than 1 pattern per class.                                                 |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|     window                 | None                    | Max allowed window for time shift                                                                                                               |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|     max_dist               | None                    | Max Distance. Stop distance computation if above max_dist. Sets them to inf. Improves speed.                                                    |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|     use_pruning            | False                   | Euclidean distance is max_dist (Maximum distance). Improves speed.                                                                              |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|     penalty                | None                    | Apply penalty if compression/expansion is applied while matching patterns.                                                                      |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|  optimization_parameters   |   Dict                  |  Add any dtw parameter you want to optimize in there. Provide parameters as list like, "window": [0, 1, 2]                                      |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
| **Random Forest parameters (json: randomforest)**       Look up further parameter description on scikit-learn.org                                                                                      |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|     n_estimators           | 100                     | The number of trees in the forest.                                                                                                              |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|     criterion              | "gini"                  | Quality measure of split. Choose "gini" for the Gini impurity and "entropy" for the information gain. Note: this parameter is tree-specific.    |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|    max_depth               |  null                   | The maximum depth of the tree. None means full expansion all all leaves are pure or until all leaves contain less than min_samples_split        |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|    min_samples_splits      |   2                     | The minimum number of samples required to split an internal node:                                                                               |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|    min_samples_leaf        |   1                     | The minimum number of samples required to be at a leaf node.                                                                                    |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|  min_weight_fraction_leaf  |    0.0                  | The minimum weighted fraction of the sum total of weights (of all the input samples) required to be at a leaf node.                             |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|   max_features             |   auto                  | The number of features to consider when looking for the best split:                                                                             |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|   max_leaf_nodes           |   null                  | Grow trees with max_leaf_nodes in best-first fashion.                                                                                           |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|  min_impurity_decrease     |    0.0                  | A node will be split if this split induces a decrease of the impurity greater than or equal to this value.                                      |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|  bootstrap                 |   True                  | Whether bootstrap samples are used when building trees. If False, the whole dataset is used to build each tree.                                 |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|  oob_score                 |    False                | Whether to use out-of-bag samples to estimate the generalization score. Only available if bootstrap=True.                                       |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|  random_state              |   null                  | Controls both the randomness of the bootstrapping of the samples used when building trees and for sampling of features                          |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|  verbose                   |   0                     | Controls the verbosity when fitting and predicting.                                                                                             |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|  class_weight              |   null                  | Weights associated with classes in the form {class_label: weight}. If not given, all classes are supposed to have weight one.                   |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|  ccp_alpha                 |   ccp_alpha             | Complexity parameter for Minimal Cost-Complexity Pruning. subtree with largest cost complexity that is smaller than ccp_alpha will be chosen.   |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|  max_samples               |   null                  | If bootstrap is True, the number of samples to draw from X to train each base estimator.                                                        |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+
|  optimization_parameters   |   Dict                  | Add any random forest parameter you want to optimize in there. Provide parameters as list like, "n_estimators": [30, 100, 200]                  |
+----------------------------+-------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------+


Further informations on
- random forest: https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html
- dynamic time warping: https://dtaidistance.readthedocs.io/en/latest/index.html


Example Config File
======================================

.. code-block:: json

    {
        "app": {
            "algorithm": "knn_dtw",
            "window": 512,
            "model": null,
            "model_save": false,
            "samples": "/workspace/test/samples_ts.pkl",
            "log_level": "DEBUG",
            "threads": -1,
            "imputation": true,
            "imputation_strategy": "interpolate",
            "imputation_constant": -9999,
            "rasters_are_timeseries": true
        },
        "supervised": {
            "probability": true,
            "all_probabilities": false,
            "remove_outliers": true,
            "optimization": {
                "optimize": false,
                "optimize_number": 10
            }
        },
        "unsupervised": {
            "nclasses": 2,
            "trainfraction": 1.0
        },
        "accuracy": {
            "perform_assesment": true,
            "testfraction": 0.5
        },
        "dtw": {
            "patterns": null,
            "patterns_save": false,
            "number_of_patterns_per_class": 1,
            "n_neighbors": 1,
            "window": null,
            "max_dist": null,
            "use_pruning": false,
            "penalty": null,
            "optimization_parameters": {
                "n_neighbors": [
                    1
                ],
                "number_of_patterns_per_class": [
                    1
                ],
                "window": [
                    null
                ],
                "penalty": [
                    null
                ]
            }
        },
        "randomforest": {
            "n_estimators": 100,
            "criterion": "gini",
            "max_depth": null,
            "min_samples_split": 2,
            "min_samples_leaf": 1,
            "min_weight_fraction_leaf": 0.0,
            "max_features": "auto",
            "max_leaf_nodes": null,
            "min_impurity_decrease": 0.0,
            "bootstrap": true,
            "oob_score": false,
            "random_state": null,
            "verbose": 0,
            "class_weight": null,
            "ccp_alpha": 0.0,
            "max_samples": null,
            "optimization_parameters": {
                "max_features": [
                    "auto",
                    "sqrt",
                    "log2"
                ],
                "max_leaf_nodes": [
                    3,
                    5,
                    7
                ],
                "max_depth": [
                    null,
                    1,
                    3,
                    10,
                    20000
                ],
                "n_estimators": [
                    10,
                    50,
                    100
                ],
                "min_samples_split": [
                    2
                ],
                "min_samples_leaf": [
                    1
                ],
                "min_weight_fraction_leaf": [
                    0.0
                ],
                "min_impurity_decrease": [
                    0.0
                ],
                "oob_score": [
                    false
                ],
                "class_weight": [
                    null
                ],
                "ccp_alpha": [
                    0.0
                ],
                "max_samples": [
                    null
                ]
            }
        }
    }
