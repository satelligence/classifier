======================================
Frequently Asked Questions
======================================

**I get an error with gathering samples! Is this a bug?**

It might be. But in most cases, this means there is an issue with your polygons.
Use a GIS programme (like QGIS or ArcGIS) to validate your polygons. If you
are convinced that your polygons are ok, then submit an issue and supply a
(subset) of your polygon file so we can take a look at the problemn.

**Classifier creates all files as root! Can I do something about this?**
Yes, you can. Run the classifier image with the following option and it will
create files with your own username!

``--user $(id -u):$(id -g)``