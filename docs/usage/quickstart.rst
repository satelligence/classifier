==================
Quickstart
==================
For the examples below we assumed that you have *tagged* the latest
version as **classifier** using ``docker tag``

Start with asking for help::

     docker run classifier --help

``--help`` Shows you the three available modes for classifier. If you want to
get more information on any of the modules (for example classification)::

  docker run classifier classification --help

Now the help shows you the arguments you need to provide to do a classification.
Only one (or multiple) raster files are really necessary. The name is
optional and whether or not you provide training data (``--rois``) depends on
your application.

Do anything but ask for help, and you first need to point it to a folder
on your system to use as your classifier *workspace*. Create a workspace
folder in a convenient location on your system (for instance your users home
folder or your data drive). This can be done with a graphical file explorer,
or using the ``cd`` and ``mkdir`` command available on most platforms from
your shell. Avoid using spaces or any other complex characters, such as
periods or integers::

    cd convenient_location
    mkdir my_first_classification

To connect your workspace to classifier, use the docker run ``-v`` option and
refer to the files in your workspace, relative to the workspace folder. For
instance if your file is called `data.tif`, it sits in a folder `input` in your
workspace ``/home/user/workspace``::

    docker run -v /home/user/my_first_classification:/workspace classifier classifcation input/data.tif


Substitute `/home/user/my_first_classification` with your absolute location. On
Windows this starts with the letter of the drive, such as `c:/` or `d:/`.

----------------------------
Test run
----------------------------
To perform a test run on some sample data, start with creating a workspace and
an input folder::

    mkdir workspace cd workspace mkdir rasters

Download a sample tif_ to the folder you just created.

.. _tif: https://storage.cloud.google.com/s11-litmustest/Peninsular_Malaysia_2016_1_Landsat8.tif?hl=nl&organizationId=861588583110&_ga=2.132769294.-1469647224.1533020557

Now run Classifier with the rasters folder as input::

    docker run -v /home/user/workspace:/workspace classifier classification rasters --name sample_run


Find your classification output in `/home/user/workspace/sample_run/`. You
will see that the classification only has 1 class! This is because the
default settings have not been changed to meet our needs. Read more about all
the parameters you can set in the config file section.