===============
Installation
===============

Classifier runs in a *Docker container*. You need to install *Docker* for your
platform to run containers. Follow the installation instructions from the Docker
website: https://docs.docker.com/install

Docker is a command line application. Run it from *powershell* on Windows,
*terminal* on Ubuntu or any other *interactive shell* on your platform.

After you have successfully installed Docker on your platform, check whether
Docker is functioning well by running it with the help option in your shell::

    docker --help

This should output a brief description of all the docker capabilities.

NOTE for unix users: if you do not have permissions to connect to the docker
daemon, add yourself to the docker group::


    usermod -a -G docker $USER


After doing this, you need to logout from your session and log back in.

Now *pull* the classifier *image*, to download the application::


    docker pull registry.gitlab.com/satelligence/classifier

Classifier is versioned. Pulling *registry.gitlab.com/satelligence/classifier*
will give you the default version called *latest*, the bleeding edge version.
It's good practice to select a specific version. Visit the repository's
registry_ to find
all available versions.

When you succesfully pulled the container you will see it at the top of the
Docker image list output::


    docker image ls


The image has an inconveniently long name, give it a short name by *tagging*
it::


    docker tag registry.gitlab.com/satelligence/classifier:latest classifier

.. _registry: https://gitlab.com/satelligence/classifier/container_registry