.. Classifier documentation master file, created by
   sphinx-quickstart on Tue Oct 30 14:06:02 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Classifier documentation
======================================
Welcome to the Classifier documentation. If you find that information is
missing, please let us know via an issue on our gitlab page_.

.. toctree::
   usage/installation
   usage/quickstart
   usage/tutorial/tutorials
   usage/configfile
   usage/FAQ


.. _page: https://gitlab.com/satelligence/classifier