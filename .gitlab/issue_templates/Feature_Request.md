### Summary

(Summarize what you would like to see)




### Example of expected outcome

(If possible, show here something which should be the expected outcome of the
new feature)


### Relevant information, links or example code snippets

(Paste any relevant information, links to webpages or example code for the feature
in here. When putting code, make sure to use code blocks (```), because otherwise
it hampers legibility)

### Make sure to assign someone to the feature request and give it a weight!
/label ~"Feature Request"
