### Summary

(Summarize the bug encountered concisely)


### Steps to reproduce

(How one can reproduce the issue - this is very important)


### Example Project/Data

(If possible, please link your input data from google buckets/drive (prefarably buckets)
which exhibit the problematic behaviour.


### Bug behaviour
#### What is the current bug behavior?

(What actually happens)


#### What is the expected correct behavior?

(What you should see instead)


### Relevant logs and/or screenshots

(Paste any relevant logs - please use code blocks (```) to format console output,
logs, and code as it's very hard to read otherwise.)


### Possible fixes

(If you can, link to the line of code that might be responsible for the problem)

### Make sure to assign someone to the bug and give it a weight
The weight signifies importance (1, annoying but doesnt influence results to 
10: I cannot continue my work and it is essential)
/label ~"Bug"
