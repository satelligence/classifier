# Merge patch release back to master

<!-- markdownlint-disable MD033 -->
<details>
<summary>This merge request will merge a patch release back to master.</summary>

Check the commit log to see what fixes are going to be merged :)

The name of the merge request includes the source branch of fixes.

</details>
<br/>
<!-- markdownlint-enable MD033 -->

Important!: **Please don't squash this merge request!**

## Status

_Master pipeline status:_
[![pipeline status](https://gitlab.com/satelligence/classifier/badges/master/pipeline.svg)](https://gitlab.com/satelligence/classifier-/commits/master)

### Remarks

Important!: **Please don't squash this merge request!**

> NB: this merge request description is a draft and will likely be changed :)
> See `.gitlab/merge_requests/merge_request_post_release.md` for details and
> proposing changes :v:
