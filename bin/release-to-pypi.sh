#!/bin/bash
# apt install python3-pip
# pip3 install twine # as sudo

RELEASE=$1
TWINE_USERNAME=$2
TWINE_PASSWORD=$3

python3 setup.py sdist bdist_wheel

if [ $RELEASE = "RELEASE" ]
then
    twine upload --verbose dist/* -u $TWINE_USERNAME -p $TWINE_PASSWORD
elif [ $RELEASE = "DEV" ]
then
    twine upload --verbose --repository testpypi dist/* -u $TWINE_USERNAME -p $TWINE_PASSWORD
else
    echo "Unknown option $RELEASE"
fi
