#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# (c) Satelligence, see LICENSE.rst.

# fail on error
set -e

FILE_PATH="$1"
FAIL_OVER="$2"

# Run MyPy
{ mypy --disallow-untyped-defs $FILE_PATH || true; } | tee mypy.out

# Check number of errors
ERRORS=`grep -o "Found .* errors" mypy.out | grep -o " .* "`

echo "Number of errors: $ERRORS, threshold: $FAIL_OVER."

if [ "$ERRORS" -gt "$FAIL_OVER" ]; then
    echo "Too many errors! Fail."
    exit 1
fi