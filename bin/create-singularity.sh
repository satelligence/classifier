#!/bin/bash

VERSION=$1
DOCKER_IMAGE="docker://registry.gitlab.com/satelligence/classifier:${VERSION}"
SIF_IMAGE="classifier_${VERSION}.sif"


singularity build $SIF_IMAGE $DOCKER_IMAGE

echo "Copying sif images to Google Storage"
gsutil cp $SIF_IMAGE gs://s11-production-dprof-models/sif/${SIF_IMAGE}
gsutil cp $SIF_IMAGE gs://s11-staging-dprof-models/sif/${SIF_IMAGE}
