#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# (c) Satelligence, see LICENSE.rst.

# fail on error
set -e

# this is required to give the git client some authentication details
git config --global user.email $GITLAB_USER_EMAIL
git config --global user.name $GITLAB_USER_NAME

# this is required to allow PUSH operations to the GitLab
# GITLAB_PRIVATE_TOKEN should be defined in the repository settings
# read more at https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html
git remote set-url origin https://"autorelease:${GITLAB_PRIVATE_TOKEN}"@gitlab.com/$CI_PROJECT_PATH
git fetch origin $CI_COMMIT_BRANCH && git checkout $CI_COMMIT_BRANCH

RELEASE_VERSION=`grep -oP "[0-9\.]+(?=\.dev0)" setup.py`
echo "About to release version $RELEASE_VERSION"

# Create release
echo "Creating release"
echo "[zest.releaser]" >> ~/.pypirc
echo "release = no" >> ~/.pypirc
echo "push-changes = no" >> ~/.pypirc
fullrelease --no-input && git push origin $CI_COMMIT_BRANCH --tags

# Major/minor release
if [[ $RELEASE_VERSION =~ ^([0-9]*)\.([0-9]*)\.0$ ]]; then

echo "Merge back to $CI_DEFAULT_BRANCH"
git fetch origin $CI_DEFAULT_BRANCH && git checkout $CI_DEFAULT_BRANCH
git pull origin $CI_COMMIT_BRANCH
git push origin $CI_DEFAULT_BRANCH

export NEXT_RELEASE=${BASH_REMATCH[1]}.$((BASH_REMATCH[2]+1))
echo "Back to development ${NEXT_RELEASE}.0"
sed -i "s/^version = .*$/version = '${NEXT_RELEASE}.0.dev0'/" setup.py
sed -i "s/^[0-9\.]* (unreleased)$/${NEXT_RELEASE}.0 (unreleased)/" CHANGES.rst
git commit -am "Back to development ${NEXT_RELEASE}.0"
git push origin $CI_DEFAULT_BRANCH
export BRANCH_SLUG=fixes_${NEXT_RELEASE}

echo "Will create branch fixes_$BRANCH_SLUG"
git checkout -b $BRANCH_SLUG
git push origin $BRANCH_SLUG

echo "Creating merge request for release ${NEXT_RELEASE}.0"
gitlab_auto_mr \
  --target-branch=$BRANCH_SLUG \
  --source-branch=$CI_DEFAULT_BRANCH \
  --commit-prefix="Release ${NEXT_RELEASE}.0" \
  --description=./.gitlab/merge_request_templates/merge_request_release.md

# Patch release
else

echo "Will create branch patch_$RELEASE_VERSION"
git checkout $RELEASE_VERSION
git checkout -b patch_$RELEASE_VERSION
git push origin patch_$RELEASE_VERSION

echo "Creating merge request to merge patch release $RELEASE_VERSION back to $CI_DEFAULT_BRANCH"
gitlab_auto_mr \
  --target-branch=$CI_DEFAULT_BRANCH \
  --source-branch=patch_$RELEASE_VERSION \
  --commit-prefix="Merge back to $CI_DEFAULT_BRANCH" \
  --description=./.gitlab/merge_request_templates/merge_request_post_release.md \
  --remove-branch
fi
