FROM ghcr.io/osgeo/gdal:ubuntu-small-3.10.2

# Install libspatialindex and python pip
RUN apt-get update && apt-get install -y libspatialindex-dev \
    python3-pip python3.12-venv \
    libomp-dev \
    git

# Create classifier workspace
RUN mkdir /workspace

# Add application logic
ADD classifier/ /app/classifier
ADD bin/ /app/bin
ADD docs/ /app/docs
ADD tests/ /app/tests

# Copy the application's requirements.txt and run pip to install all
# dependencies. Remove build artifact.
ADD requirements_dev.txt /app/requirements_dev.txt
ADD setup.py /app/setup.py
ADD README.md /app/README.md
WORKDIR /app

# In Ubuntu 22.04, pip can't install packages in the system python, only in virtual envs
# Create and activate virtual environment
RUN python -m venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"

# Install dependencies in virtual environment
RUN python -m pip install --upgrade pip setuptools
RUN python -m pip install -e .

# Remove line in matplotlibrc stating "backend: tkinter" crap.
RUN sed -i '/^backend/ d' /opt/venv/lib/python3.12/site-packages/matplotlib/mpl-data/matplotlibrc

ADD pylintrc /app/pylintrc
ADD mypy.ini /app/
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

ARG git_tag
ENV VERSION=${git_tag}
# Joblib pickler cause cloudpickle does not work well for large objects
ENV LOKY_PICKLER=pickle
ENTRYPOINT ["python3", "-m" , "classifier.cli"]
